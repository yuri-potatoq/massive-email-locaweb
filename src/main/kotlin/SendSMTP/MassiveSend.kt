package SendSMTP


import SendSMTP.db.DBConnection
import SendSMTP.model.Body
import SendSMTP.model.ResponseFields
import SendSMTP.service.Locaweb
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


class MassiveSend(
    val socketClient: Connections,
    val target: Counter
){
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.smtplw.com.br/v1/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val callService = retrofit
        .create(Locaweb::class.java)

    private val db = DBConnection(
        database ="mydb",
        port ="5432",
        realm ="",
        user = "postgres"
    )

    private fun requirer(body: Body, count: Counter) {

        callService.emailResponse(body).enqueue(object: Callback<ResponseFields>{
            override fun onResponse(
                call: Call<ResponseFields>,
                response: Response<ResponseFields>
            ){
                if (response.isSuccessful) {

                    System.out.println("Response OK")

                    response.body()?.let {
                        /** Escreve na stream, uma instância ResponseFields */
//                        socketClient.writeData(it)
                        println(it.data.attributes.to)

                        /** Insere os dados na DB */
                        db.pgInsertOnResponse(it)

                        /** Decrementa o número de chunks */
                        count.decrementCounter()

                    }

                } else {
                    System.out.println("Response ERROR " + response.message())
                    println(response.raw())

                    db.pgInsertOnFailure(body)
                    count.decrementCounter()

                }
            }

            override fun onFailure(
                call: Call<ResponseFields>,
                t: Throwable
            ) {
                System.out.println("Require ERROR")
                socketClient.write("ERROR")

                db.pgInsertOnFailure(body)

                count.decrementCounter()
            }

        })

    }

    fun massiveSend(
        emails: List<String>,
        body: String,
        from: String
    ): Boolean {

        val chunkedList = emails.size
        var observableCounter = Counter(chunkedList)

        observableCounter.addObserver(object : Observer{
            override fun update(o: Observable?, arg: Any?) {
                var count = arg as Int
                if(count<=0) target.decrementCounter()
            }
        })

        for (mail in emails){
            this.requirer(Body(
                to = mail,
                from = from,
                body = body,
                "Sea Telecom"
            ), observableCounter)
        }

        /*


        var aux = 0

        for (item in emails) {
            if (aux >= 500) {
                Thread.sleep(3000L)
            }
            
            requirer(Body(
                to = item,
                from = from,
                body = body,
                "Eu"
            ))
            aux++;
        }
*/
        return true
    }


}
