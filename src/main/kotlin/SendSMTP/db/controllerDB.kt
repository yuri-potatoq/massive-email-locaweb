package SendSMTP.db


import SendSMTP.model.Body
import SendSMTP.model.ResponseFields
import kotlinx.coroutines.TimeoutCancellationException
import java.sql.DriverManager
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import kotlin.random.Random


class DBConnection(
    val database: String,
    val port: String,
    val realm: String,
    val user: String
){
    private lateinit var conn: Connection
    private val tableName: String = "emails_sent"
    private val addr: String = System.getenv("MASSIVE_EMAIL_DB_ADDR")

    init {
        try {
            Class.forName("org.postgresql.Driver")
            this.conn = DriverManager.getConnection(
                "jdbc:postgresql://$addr:$port/$database?user=admin&password=admin"
            )

            conn.createStatement().executeUpdate(
                """CREATE EXTENSION IF NOT EXISTS "uuid-ossp";""")

            conn.createStatement().executeUpdate("""
                CREATE TABLE IF NOT EXISTS $tableName(
                    id uuid DEFAULT uuid_generate_v4 (),
                    post_id VARCHAR(30) NOT NULL, 
                    target VARCHAR(100) NOT NULL, 
                    subject VARCHAR(100) NOT NULL, 
                    description VARCHAR(100), 
                    title VARCHAR(100), 
                    status VARCHAR(45) NOT NULL, 
                    launch TIMESTAMP, 
                    PRIMARY KEY (id)
                );""".trimIndent()
            )


//            this.conn ?: throw Throwable()

        } catch(timeout: TimeoutCancellationException){
            println("Conexão com o banco de dados não efetuada!")
        }
    }

    fun pgInsertOnResponse(responseObject: ResponseFields){

        val statement = conn.prepareStatement("""
        INSERT INTO $tableName (
            post_id, target, subject, description, title, status, launch )
        values(?, ?, ?, ?, ?, ? , current_timestamp(0));
        """.trimIndent())

        statement.setString(1, responseObject.data.id.toString())
        statement.setString(2, responseObject.data.attributes.to)
        statement.setString(3, responseObject.data.attributes.subject)
        statement.setString(4,  responseObject.data.links.self)
        statement.setString(5,  "Pesquisa de satisfação")
        statement.setString(6, responseObject.data.attributes.status)

        statement.execute()
    }


    fun pgInsertOnFailure(body: Body){
        val statement = conn.prepareStatement("""
        INSERT INTO $tableName (
            post_id, target, subject, description, title, status, launch )
        values(?, ?, ?, ?, ?, ? , current_timestamp(0));
        """.trimIndent())


        statement.setString(1, "0000")
        statement.setString(2, body.to)
        statement.setString(3, body.subject)
        statement.setString(4, "Falha no envio")
        statement.setString(5,  "Falha no envio")
        statement.setString(6, "Falha no envio")

        statement.execute()
    }

    fun insertTest(body: Body){

    }

    fun pgSelect(query: String): ResultSet {
        val statement = this.conn.createStatement()!!
        return statement.executeQuery(query)

    }

}


