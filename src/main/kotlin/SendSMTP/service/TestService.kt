package SendSMTP.SendSMTP.service

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


interface TestService {
    @POST("tools/massive_emails/test")
    fun testSender(
        @Body
        body: SendSMTP.model.Body
    ): Call<SendSMTP.model.Body>

}