package SendSMTP


import SendSMTP.SendSMTP.service.TestService
import SendSMTP.db.DBConnection
import SendSMTP.model.Body
import SendSMTP.model.ResponseFields
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


class Test(
    val socketClient: Connections,
    val counter: Counter
){
    private val retrofit = Retrofit.Builder()
        .baseUrl("http://0.0.0.0:5500/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val callService = retrofit
        .create(TestService::class.java)

    private val db = DBConnection(
        database ="mydb",
        port ="5432",
        realm ="",
        user = "postgres"
    )

    private fun requirer(body: Body) {

        callService.testSender(body).enqueue(object: Callback<Body>{
            override fun onResponse(
                call: Call<Body>,
                response: Response<Body>
            ){
                if (response.isSuccessful) {

                    System.out.println("Response OK")

                    response.body()?.let {
                        /** Escreve na stream, uma instância ResponseFields */
                        counter.decrementCounter()
                        println(response.code())
                        print(call.request().body().toString())
                        db.insertTest(it)
                    }

                } else {
                    System.out.println("Response ERROR " + response.message())
                    println(response.raw())
                    socketClient.clientClose()
                }
            }

            override fun onFailure(
                call: Call<Body>,
                t: Throwable
            ) {
                println("Require ERROR")
            }

        })

    }


    fun massiveSend(
        emails: Array<String>,
        body: String,
        from: String
    ): Boolean {

//        var aux = 0
//        for (item in emails) {
//            if (aux >= 500) {
//                Thread.sleep(3000L)
//            }
//
//            requirer(Body(
//                to = item,
//                from = from,
//                body = body,
//                "Eu"
//            ))
//            aux++;
//        }

        return true
    }

//    fun massiveSend(): Boolean {
//        var aux = 0
//
//        while (counter.amount > 0){
//            if (aux >= 500) {
//                aux = 0
//                Thread.sleep(3000L)
//                println("CHUNK SENT")
//                socketClient.write("CHUNK SENT")
//            }
//
//            requirer(Body(
//                to = "alguém",
//                from = "yuri.ylr@outloo",
//                body = "{'status': 'to bebo'}",
//                "Eu"
//            ))
//
//            println(counter.amount)
//            aux++
//        }
//
//        socketClient.write("ALL CHUNKS SENT")
//        return true
//    }

}
