package SendSMTP


import SendSMTP.model.ResponseFields
import com.google.gson.Gson
import kotlinx.coroutines.withTimeout
import kotlinx.serialization.Serializable
import java.net.InetAddress
import java.net.ServerSocket
import java.io.*
import java.lang.StringBuilder
import java.net.Socket
import java.util.Queue
import java.util.LinkedList
import java.util.Observer
import java.util.Observable
import kotlin.math.ceil

/**
 * Modificado 28/01
 * */
class Connections(
    private val server: ServerSocket
){
    private val client: Socket
    private val gson = Gson()
    var input: BufferedReader
    val output: OutputStreamWriter

    init {
        client = this.server.accept()

        input = BufferedReader(
            InputStreamReader(client.getInputStream())
        )
        output = OutputStreamWriter(
            client.getOutputStream(), "UTF-8"
        )

        System.out.println(
        """
            Conexão em ${client.inetAddress.hostAddress}
            channel: ${client.channel}
            port: ${client.port}
            Thread: ${Thread.currentThread().name}
        """.trimIndent())
    }

    fun clientClose(){
        client.close()
    }

    /* Lê do inputStream */
    fun read(): Request {

//        val resp = gson.fromJson(
//            input.lines().collect(Collectors.joining()),
//            Request::class.java
//        )
        val resp = StringBuilder()

        while(true){
            val ch = input.read()
            if (ch == -1) break;
            if (ch == 125) {
                resp.append((125).toChar())
                break
            }
            resp.append(ch.toChar())
        }
        println("gotcha")
        return gson.fromJson(resp.toString(), Request::class.java)
    }

    fun write(response: String){
        /* Escreve no outputStream */
        output.write(this.gson.toJson(
            response
        ))
        output.flush()
    }

    fun writeData(response: ResponseFields){
        /* Escreve no outputStream */
        response.data.attributes.body = "default"
        output.write(gson.toJson(response))
        output.flush()
    }

    @Serializable
    data class Response(
        val request: String,
        val task: String,
        val state: String
    )

    @Serializable
    data class Request(
        val service: String,
        val template: String,
        val emails: String,
        val sender: String
    )

}


class Counter(
    var amount: Int
): Observable(){

    fun decrementCounter(){
        amount--
        setChanged()
        notifyObservers(amount)
    }
}

class ManagerCounter(
    val client: Connections
): Observer{

    override fun update(p0: Observable?, p1: Any?) {
        p1?.let{
            println(it)
           if (it as Int <= 0) {
               println("ALL FINISHED")
               client.clientClose()
           }
        }
    }
}

fun main(args: Array<String>){

    val server = ServerSocket(
        6060, 50, InetAddress.getByAddress(byteArrayOf(0,0,0,0))
    )

    while (true) {
        val client = Connections(server)

        val response = client.read()
        println(response.service)

        /** Multiplexação dos serviços à serem executados. */
        when(response.service){
            "massiveSendEmails" -> {
                val emailQueue = scrapyCsv(response.emails)
                val chunkSize: Int = 500

                val template = takeTemplate(response.template)

                /* Verifica o total de chunks a serem usados */
                var totalChunk: Int = if ((emailQueue.size % chunkSize) == 0)
                {
                    emailQueue.size / chunkSize
                }else {
                    ceil(emailQueue.size / chunkSize.toDouble()).toInt()
                }

                val chunkedList = emailQueue.chunked(chunkSize)
                var observableCounter = Counter(totalChunk)

                val massiveSendInstance = MassiveSend(
                    client, observableCounter
                )

                observableCounter.addObserver(object : Observer{
                    override fun update(o: Observable?, arg: Any?) {
                        var count = arg as Int

                        if (count >= 0) {
                            massiveSendInstance.massiveSend(
                                emails = chunkedList.get(count),
                                from = response.sender,
                                body = template
                            )
                            println("CHUNK SENT")
                            client.write("SENT ${totalChunk - count} / $totalChunk")
                        }else{
                            client.write("FIN")
                            client.clientClose()
                        }
                    }
                })

                observableCounter.decrementCounter()
            }
            "test" -> {
//                val target = Counter(1000)
//                target.addObserver(ManagerCounter(client))
//
//                val clientInstance = Test(client, target)
//                clientInstance.massiveSend()
//
//
//                println("FIM DO ESCOPO")
//                client.clientClose()
            }
        }
    }

}

/**
 * Função que enfileira os emails recebidos por csv
 * em uma Queue à ser decrementada.
 */

fun scrapyCsv(path: String): ArrayList<String>{
    val list = ArrayList<String>()

    val buff = BufferedReader(FileReader(File(path))).use {
        try {
            while (it.ready()){
                val line = it.readLine().split(",")
                list.addAll(line)
            }

        }catch (e: IOException) {
            println("Falha ao ler CSV")
        }
    }

    return list
}

/**
 * Função na qual faz o build de uma string
 * contendo o template do path submetido.
 */
fun takeTemplate(path: String): String{
    val buffString  = StringBuilder()

    val buff = BufferedReader(FileReader(File(path))).use {
        while (it.ready()) {
            buffString.append(it.readLine())
        }
    }

    return buffString.toString()
}
